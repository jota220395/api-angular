export class Movie{
/*     las interrogaciones son por si la película no tiene algún dato
 */ Title?:string;
    Year?:string;
    Runtime?:string;
    Released?:string;
    Genre?:string;
    Director?:string;
    Poster?:string;
    imdbVotes?:string;
    Plot?: string;
}