import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../services/peliculas.service';
import { trigger, transition, style, animate } from '@angular/animations';
/* import { Movie } from '../models/movie.model';
 */

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.sass'],
  providers: [PeliculasService],
  animations: [
    trigger('fade', [
      transition('void => active', [
        style({ opacity: 0 }),
        animate(1000, style({ opacity: 1 }))
      ]),
      transition('* => void', [
        animate(1000, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class BusquedaComponent implements OnInit {
  public titulo:String;
  public datos:Array<any> = [];

  constructor(private _peliculasService:PeliculasService) {
    this.titulo;
    //Recogemos los datos de la pelicula
  }
//Devuelve la url que recoge el input del Form. Esta contiene un json con la info
  onSubmit(busqueda:any){
     if(typeof this.titulo == undefined){ 
       this.titulo="star"}
    this._peliculasService.obtener(this.titulo).subscribe(data=>{
      this.datos = data.Search;
    },
    error =>{
      var errorMessage = error as any;
      console.log(errorMessage);
    })
  
  }
  ngOnInit(): void {
  }

}
