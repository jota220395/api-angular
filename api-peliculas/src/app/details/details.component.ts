import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../services/peliculas.service';
import { ActivatedRoute } from  '@angular/router';
import { Movie } from '../models/movie.model';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass'],
  providers: [PeliculasService],
  animations: [
    
  ]
})
export class DetailsComponent implements OnInit {
  public titulo:String;
  public datos:Array<any> = [];

  constructor(private activatedRoute: ActivatedRoute, private _peliculasService:PeliculasService) {
    
  }
//Devuelve la url que recoge el form. Esta contiene un json con información de las películas
  
movie:Movie = new Movie;

  async ngOnInit(){
    console.log(this.activatedRoute.snapshot.params.id);
    try{
      const req: any = await this._peliculasService.getMovie(this.activatedRoute.snapshot.params.id)
      if(req.Response == 'True'){
        this.movie = req;
        console.log(req);
      }
    } catch(e){
console.log(e);
    }
  }

}