import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PresentationComponent } from './presentation/presentation.component';
import { Page404Component } from './page404/page404.component';
import { DetailsComponent } from './details/details.component';
import { BusquedaComponent } from './busqueda/busqueda.component'



const routes: Routes = [
   {path: '', component: PresentationComponent},
   { path: 'busqueda', component: BusquedaComponent },
   {path: 'busqueda/detalles/:id', component:DetailsComponent},
   {path: '**', component: Page404Component}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
